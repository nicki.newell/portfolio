---
title: "About"
date: 2017-08-31T20:40:14-05:00
draft: false
---

A young artist living life with her husband, cuddly cat and anxious dog in beautiful downtown Plano, TX.
