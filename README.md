# How to Start

Open Git Bash

```
cd /c/repos/portfolio
code .
```

## How to Run Site locally

```
hugo server -Dw 
```

## Publish

Make sure all of your changes to the app are *committed*. To check, type `gs` in git bash, if there are red things, add them with `gaa`, if there are green things, commit them with `git commit -m "This is what I did"`.

To publish your site to *netlify* you just need to push your changes:

```
git push
```

If you get an authentication error make sure you have SSH agent running with your default site added

```
ags
aga
```

## [Osprey Theme](https://themes.gohugo.io/osprey/)
